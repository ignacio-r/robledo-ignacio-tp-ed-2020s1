//
// Created by ignacio on 11/7/20.
//

#include <cstdlib>
#include "InmutableLinkedList.h"


//Inv: las listas se representan como un puntero a listarepr que no es null.

struct Node {
    int value;
    Node *next;
};

struct InmListRepr {
    Node *first;
};

InmList emptyInmList() {
    InmList lista = new InmListRepr;
    lista->first = NULL;
    return lista;
}

bool isEmpty(InmList list) {
    return list->first == NULL;
}

InmList cons(int x, InmList list) {
    Node *nodo = new Node;
    nodo->value = x;
    nodo->next = list->first;
    InmList inmList = new InmListRepr;
    inmList->first = nodo;
    return inmList;
}

int head(InmList list) {
    return list->first->value;
}

InmList tail(InmList list) {
    InmList inmList = new InmListRepr;
    inmList->first = list->first->next;
    return inmList;
}


/*int main(){
    return 0;
};*/
