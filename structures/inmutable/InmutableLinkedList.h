//
// Created by ignacio on 11/7/20.
//

#ifndef STRUCTURES_INMUTABLELINKEDLIST_H
#define STRUCTURES_INMUTABLELINKEDLIST_H
typedef struct InmListRepr *InmList;

InmList emptyInmList();

InmList cons(int x, InmList list);

bool isEmpty(InmList list);

int head(InmList list);

InmList tail(InmList list);


#endif //STRUCTURES_INMUTABLELINKEDLIST_H
