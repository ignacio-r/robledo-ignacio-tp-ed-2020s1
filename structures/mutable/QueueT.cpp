//
// Created by ignacio on 21/7/20.
//
using namespace std;

#include <cstdlib>
#include <iostream>
#include "QueueT.h"

struct Node {
    ElemQT elem;
    Node *next;
};
/*
 * Inv:
 * si first es null last también
 * len es cantidad de nodos desde first hasta last
*/
struct QueueTSt {
    Node *first;
    Node *last;
    Node *current;
    int length;
};

QueueT emptyQT() {
    QueueT queue = new QueueTSt;
    queue->first = NULL;
    queue->last = NULL;
    queue->current = NULL;
    queue->length = 0;
    return queue;
}

bool isEmptyQT(QueueT queue) {
    return queue->length == 0;
}

void addLastQT(Node *currentNode, Node *newNode) {
    while (currentNode->next != NULL) {
        currentNode = currentNode->next;
    }
    currentNode->next = newNode;
}

//prec: current node no es null
//O(1)
void queueQT(ElemQT x, QueueT queue) {
    Node *nodePointer = new Node;
    nodePointer->elem = x;
    nodePointer->next = NULL;
    if (queue->first == NULL) {
        queue->first = nodePointer;
        queue->last = nodePointer;
    } else {
        queue->last->next = nodePointer;
        queue->last = nodePointer;
    }
    queue->length++;
}

void printQT(QueueT queue) {
    cout << "Queue: " << endl;
    cout << " first: " << queue->first << endl;
    cout << " last: " << queue->last << endl;
    cout << " length: " << lenQT(queue) << endl;
    cout << " nodes: " << endl;
    Node *currentNode = queue->first;
    while (currentNode != NULL) {
        cout << "   dir: " << currentNode << ", ";
        cout << "elem: " << currentNode->elem << ", ";
        cout << "next: " << currentNode->next << endl;
        cout << endl;
        currentNode = currentNode->next;
    }
}

void dequeueQT(QueueT queue) {
    Node *old = queue->first;
    queue->first = queue->first->next;
    delete old;
    if (queue->first == NULL) {
        queue->last = NULL;
    }
    queue->length--;
}

ElemQT firstQT(QueueT queue) {
    return queue->first->elem;
}

int lenQT(QueueT queue) {
    return queue->length;
}

void destroyQT(QueueT queue) {
    for (int i = 0; i < 5; i++) {
        dequeueQT(queue);
    }
    delete queue;
}

//O(n)
QueueT cloneQT(QueueT q) {
    QueueT newQ = emptyQT();
    Node *currentNode = q->first;
    while (currentNode != NULL) {
        queueQT(currentNode->elem, newQ);
        currentNode = currentNode->next;
    }
    return newQ;
}

void initQT(QueueT queue) {
    queue->current = queue->first;
}

void nextQT(QueueT queue) {
    queue->current = queue->current->next;
}

ElemQT currentQT(QueueT queue) {
    return queue->current->elem;
}

bool hasNextQT(QueueT queue) {
    return queue->current != NULL;
}
