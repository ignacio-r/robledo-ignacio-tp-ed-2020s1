//
// Created by ignacio on 20/7/20.
//
using namespace std;
#include <cstdlib>
#include <iostream>
#include "Queue.h"

struct Node {
    ElemQ elem;
    Node *next;
};
/*
 * Inv:
 * si first es null last también
 * len es cantidad de nodos desde first hasta last
*/
struct QueueSt {
    Node *first;
    Node *last;
    Node *current;
    int length;
};

Queue emptyQ() {
    Queue queue = new QueueSt;
    queue->first = NULL;
    queue->last = NULL;
    queue->current = NULL;
    queue->length = 0;
    return queue;
}

bool isEmptyQ(Queue queue) {
    return queue->length == 0;
}

void addLastQ(Node *currentNode, Node *newNode) {
    while (currentNode->next != NULL) {
        currentNode = currentNode->next;
    }
    currentNode->next = newNode;
}

//prec: current node no es null
//O(1)
void queue(ElemQ x, Queue queue) {
    Node *nodePointer = new Node;
    nodePointer->elem = x;
    nodePointer->next = NULL;
    if (queue->first == NULL) {
        queue->first = nodePointer;
        queue->last = nodePointer;
    } else {
        queue->last->next = nodePointer;
        queue->last = nodePointer;
    }
    queue->length++;
}

void printQ(Queue queue) {
    cout << "Queue: " << endl;
    cout << " first: " << queue->first << endl;
    cout << " last: " << queue->last << endl;
    cout << " length: " << lenQ(queue) << endl;
    cout << " nodes: " << endl;
    Node *currentNode = queue->first;
    while (currentNode != NULL) {
        cout << "   dir: " << currentNode << ", ";
        cout << "elem: " << currentNode->elem << ", ";
        cout << "next: " << currentNode->next << endl;
        cout << endl;
        currentNode = currentNode->next;
    }
}

void dequeue(Queue queue) {
    Node *old = queue->first;
    queue->first = queue->first->next;
    delete old;
    if (queue->first == NULL) {
        queue->last = NULL;
    }
    queue->length--;
}

ElemQ firstQ(Queue queue) {
    return queue->first->elem;
}

int lenQ(Queue queue) {
    return queue->length;
}

void destroyQ(Queue queue) {
    for (int i = 0; i < queue->length; i++) {
        dequeue(queue);
    }
    delete queue;
}

//O(n)
Queue cloneQ(Queue q) {
    Queue newQ = emptyQ();
    Node *currentNode = q->first;
    while (currentNode != NULL) {
        queue(currentNode->elem, newQ);
        currentNode = currentNode->next;
    }
    return newQ;
}

void init(Queue queue) {
    queue->current = queue->first;
}

void next(Queue queue) {
    queue->current = queue->current->next;
}

ElemQ current(Queue queue) {
    return queue->current->elem;
}

bool hasNext(Queue queue) {
    return queue->current != NULL;
}