//
// Created by ignacio on 19/7/20.
//

#ifndef STRUCTURES_ARRAYLIST_H
#define STRUCTURES_ARRAYLIST_H
typedef int Elem;
typedef struct ArrayListSt *ArrayList;

ArrayList emptyList();

void addL(ArrayList list, Elem x);

int sizeL(ArrayList list);

//prec: 0 <= i < longitud
Elem nthL(ArrayList list, int index);

void destroyL(ArrayList list);


#endif //STRUCTURES_ARRAYLIST_H
