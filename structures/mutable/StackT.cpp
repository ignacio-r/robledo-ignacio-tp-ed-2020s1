//
// Created by ignacio on 3/8/20.
//

#include "StackT.h"
#include <cstdlib>

struct Node {
    ElemST elem;
    Node *next;
};

struct StackTSt {
    Node *top;
    int size;
};

StackT emptyST() {
    StackT s = new StackTSt;
    s->top = NULL;
    s->size = 0;
    return s;
}

int sizeST(StackT s) {
    return s->size;
}

bool isEmptyST(StackT s) {
    return s->size == 0;
}

void pushT(ElemST elem, StackT stack) {
    Node *newTopNode = new Node;
    newTopNode->elem = elem;
    newTopNode->next = NULL;
    if (stack->top == NULL) {
        stack->top = newTopNode;
    } else {
        Node *prevTopNode = stack->top;
        newTopNode->next = prevTopNode;
        stack->top = newTopNode;
    }
    stack->size++;
}

ElemST topT(StackT stack) {
    return stack->top->elem;
}

void popT(StackT stack) {
    if (!isEmptyST(stack)) {
        Node *prevTopNode = stack->top;
        stack->top = stack->top->next;
        delete prevTopNode;
        stack->size--;
    }
}

void destroyST(StackT stack) {
    for (int i = 0; i < stack->size; i++) {
        popT(stack);
    }
    delete stack;
}