//
// Created by ignacio on 20/7/20.
//

#ifndef STRUCTURES_TREE_H
#define STRUCTURES_TREE_H
typedef int ElemT;
struct NodeT;
typedef NodeT *Tree;

Tree emptyT();

Tree leafT(ElemT x);

bool isEmptyT(Tree tree);

Tree nodeT(ElemT x, Tree left, Tree right);

ElemT rootT(Tree tree);

Tree rightT(Tree tree);

Tree leftT(Tree tree);

void setRootT(ElemT x, Tree tree);

//prec: left debe ser empty
void setLeftT(Tree tree, Tree left);

//prec: right debe ser empty
void setRightT(Tree tree, Tree right);

//prec: la memoria de los nodos no fue liberada antes
void destroyT(Tree tree);

#endif //STRUCTURES_TREE_H
