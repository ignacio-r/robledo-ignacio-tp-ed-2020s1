//
// Created by ignacio on 21/7/20.
//

#ifndef STRUCTURES_QUEUET_H
#define STRUCTURES_QUEUET_H

#include "Tree.h"

typedef Tree ElemQT;
typedef struct QueueTSt *QueueT;

QueueT emptyQT();

bool isEmptyQT(QueueT queue);

void queueQT(ElemQT x, QueueT queue);

void dequeueQT(QueueT queue);

ElemQT firstQT(QueueT queue);

int lenQT(QueueT queue);

void printQT(QueueT queue);

void destroyQT(QueueT queue);

QueueT cloneQT(QueueT q);

//Iterator
void initQT(QueueT queue);

void nextQT(QueueT queue);

ElemQT currentQT(QueueT queue);

bool hasNextQT(QueueT queue);

#endif //STRUCTURES_QUEUET_H