//
// Created by ignacio on 3/8/20.
//

#include <cstdlib>
#include "Stack.h"

struct Node {
    Elem elem;
    Node *next;
};

struct StackSt {
    Node *top;
    int size;
};

Stack emptyS() {
    Stack s = new StackSt;
    s->top = NULL;
    s->size = 0;
    return s;
}

int sizeS(Stack s) {
    return s->size;
}

bool isEmptyS(Stack s) {
    return s->size == 0;
}

void push(Stack stack, Elem elem) {
    Node *newTopNode = new Node;
    newTopNode->elem = elem;
    newTopNode->next = NULL;
    if (stack->top == NULL) {
        stack->top = newTopNode;
    } else {
        Node *prevTopNode = stack->top;
        newTopNode->next = prevTopNode;
        stack->top = newTopNode;
    }
    stack->size++;
}

Elem top(Stack stack) {
    return stack->top->elem;
}

void pop(Stack stack) {
    if (!isEmptyS(stack)) {
        Node *prevTopNode = stack->top;
        stack->top = stack->top->next;
        delete prevTopNode;
        stack->size--;
    }
}

void destroyS(Stack stack) {
    for (int i = 0; i < stack->size; i++) {
        pop(stack);
    }
    delete stack;
}
