//
// Created by ignacio on 12/7/20.
//

#ifndef STRUCTURES_MUTABLELINKEDLIST_H
#define STRUCTURES_MUTABLELINKEDLIST_H
//Inv: lista se representa con un puntero a MListRepr que no es null
// El campo size es la longitud de la lista

typedef struct MListRepr *MList;
typedef int Elem;

MList emptyMList();

void addFirst(Elem x, MList list);

int length(MList list);

Elem nth(int x, MList list);

void destroy(MList list);

Elem takeFirst(MList list);

void addLast(Elem x, MList list);

Elem takeLast(MList list);

void addAt(Elem x, MList list, int i);

Elem takeNth(Elem x, MList list, int i);

#endif //STRUCTURES_MUTABLELINKEDLIST_H
