//
// Created by ignacio on 20/7/20.
//

#include <cstdlib>
#include "Tree.h"

struct NodeT {
    ElemT root;
    NodeT *left;
    NodeT *right;
};

Tree emptyT() {
    return NULL;
}

bool isEmptyT(Tree tree) {
    return tree == NULL;
}

Tree leafT(ElemT x) {
    return nodeT(x, emptyT(), emptyT());
}

Tree nodeT(ElemT x, Tree left, Tree right) {
    NodeT *nodePointer = new NodeT;
    nodePointer->root = x;
    nodePointer->left = left;
    nodePointer->right = right;
    return nodePointer;
}

ElemT rootT(Tree tree) {
    return tree->root;
}

Tree rightT(Tree tree) {
    return tree->right;
}

Tree leftT(Tree tree) {
    return tree->left;
}

void setRootT(ElemT x, Tree tree) {
    tree->root = x;
}

void setLeftT(Tree tree, Tree left) {
    tree->left = left;
}

void setRightT(Tree tree, Tree right) {
    tree->right = right;
}

/*void destroyT(Tree tree) {

}*/
