//
// Created by ignacio on 3/8/20.
//

#include "Tree.h"

#ifndef STRUCTURES_STACKT_H
#define STRUCTURES_STACKT_H

#endif //STRUCTURES_STACKT_H
typedef struct StackTSt *StackT;
typedef Tree ElemST;

StackT emptyST();

bool isEmptyST(StackT s);

void pushT(ElemST elem, StackT stack);

ElemST topT(StackT stack);

void popT(StackT stack);

int sizeST(StackT s);

void destroyST(StackT stack);