//
// Created by ignacio on 19/7/20.
//

#include <cstdlib>
#include "ArrayList.h"

//Inv: la capacidad representa el tamaño total del array
struct ArrayListSt {
    int capacity;
    int size;
    Elem *elements;
};

void shouldDoubleCapacity(ArrayList list);

//O(1)
ArrayList emptyList() {
    ArrayList list = new ArrayListSt;
    list->size = 0;
    list->capacity = 1;
    list->elements = new Elem[1];
    return list;
}

//O(n), un elemento es O(1) amortizado
void addL(ArrayList list, Elem x) {
    shouldDoubleCapacity(list);
    list->elements[list->size] = x;
    list->size++;
}

void shouldDoubleCapacity(ArrayList list) {
    if (list->size == list->capacity) {
        Elem *elemsPointer = new Elem[list->capacity * 2];
        for (int i = 0; i < list->capacity; i++) {
            elemsPointer[i] = list->elements[i];
        }
        list->capacity = list->capacity * 2;
        delete[] list->elements;
        list->elements = elemsPointer;
    }
}

//O(1)
int sizeL(ArrayList list) {
    return list->size;
}

//O(1)
Elem nthL(ArrayList list, int index) {
    return list->elements[index];
}

void destroyL(ArrayList list) {
    delete[] list->elements;
}
