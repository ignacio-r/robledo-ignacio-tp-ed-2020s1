//
// Created by ignacio on 12/7/20.
//

#include <cstdlib>
#include "MutableLinkedList.h"

struct Node {
    int value;
    Node *next;
};

struct MListRepr {
    Node *first;
    int size;
};

Node *goThroughNode(Node *nodePointer);

Node *getPenultimateNode(Node *nodePointer);

// O(1)
MList emptyMList() {
    MList list = new MListRepr;
    list->first = NULL;
    list->size = 0;
    return list;
}

// O(1)
void addFirst(Elem x, MList list) {
    Node *nodePointer = new Node;
    nodePointer->value = x;
    nodePointer->next = list->first;
    list->first = nodePointer;
    list->size++;
}

// O(1)
int length(MList list) {
    return list->size;
}

//prec:  0 <= i < length
//costo lineal
Elem nth(int ix, MList list) {
    Node *nodePointer = list->first;
    for (int i = 0; i < ix; i++) {
        nodePointer = nodePointer->next;
    }
    return nodePointer->value;
}

//prec: la lista no está vacía
//O(1)
Elem takeFirst(MList list) {
    Node *nodePointer = list->first;
    Elem fst = nodePointer->value;
    list->first = list->first->next;
    delete nodePointer;
    list->size--;
    return fst;
}

//O(n)
void addLast(Elem x, MList list) {
    Node *lastNode = new Node;
    lastNode->value = x;
    lastNode->next = NULL;
    list->size++;
    //empty list
    if (list->first == NULL) {
        list->first = lastNode;
    } else { //list not empty
        Node *nodePointer = list->first;
        nodePointer = goThroughNode(nodePointer);
        nodePointer->next = lastNode;
    }
}

Node *goThroughNode(Node *nodePointer) {
    while (nodePointer->next != NULL) {
        nodePointer = nodePointer->next;
    }
    return nodePointer;
}

//prec: la lista no está vacía
Elem takeLast(MList list) {
    Elem last;
    list->size--;
    //la lista tiene 1 elemento
    if (list->first->next == NULL) {
        last = list->first->value;
        delete (list->first);
        list->first = NULL;
    } else { // buscar el anteúltimo
        Node *nodePointer = list->first;
        nodePointer = getPenultimateNode(nodePointer);
        //nodePointer es el anteúltimo nodo de la lista
        last = nodePointer->next->value;
        delete nodePointer->next;
        nodePointer->next = NULL;
    }
    return last;
}

Node *getPenultimateNode(Node *nodePointer) {
    while (nodePointer->next->next != NULL) {
        nodePointer = nodePointer->next;
    }
    return nodePointer;
}

void destroy(MList list) {
    Node *nodePointer = list->first;
    while (nodePointer != NULL) {
        Node *next = nodePointer->next;
        delete nodePointer;
        nodePointer = next;
    }
    delete list;
}

//TODO
/*
//0 <= index <= length
void addAt(Elem x, MList list, int i) {

}

//prec: lista no está vacía
Elem takeNth(Elem x, MList list, int i) {
    return 0;
}

*/

