//
// Created by ignacio on 20/7/20.
//

#ifndef STRUCTURES_QUEUE_H
#define STRUCTURES_QUEUE_H


typedef struct QueueSt *Queue;
typedef int ElemQ;

Queue emptyQ();

bool isEmptyQ(Queue queue);

void queue(ElemQ x, Queue queue);

void dequeue(Queue queue);

ElemQ firstQ(Queue queue);

int lenQ(Queue queue);

void printQ(Queue queue);

void destroyQ(Queue queue);

Queue cloneQ(Queue q);

//Iterator
void init(Queue queue);

void next(Queue queue);

ElemQ current(Queue queue);

bool hasNext(Queue queue);

#endif //STRUCTURES_QUEUE_H
