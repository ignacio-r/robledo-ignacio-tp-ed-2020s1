//
// Created by ignacio on 3/8/20.
//

#ifndef STRUCTURES_STACK_H
#define STRUCTURES_STACK_H

#endif //STRUCTURES_STACK_H
typedef struct StackSt *Stack;
typedef int Elem;

Stack emptyS();

bool isEmptyS(Stack s);

void push(Stack stack, Elem elem);

Elem top(Stack stack);

void pop(Stack stack);

int sizeS(Stack s);

void destroyS(Stack stack);