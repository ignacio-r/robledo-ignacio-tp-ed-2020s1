#include <iostream>

using namespace std;

#include "inmutable/InmutableLinkedList.h"
#include "mutable/MutableLinkedList.h"
#include "mutable/ArrayList.h"
#include "mutable/Tree.h"
#include "mutable/QueueT.h"
#include "mutable/Queue.h"
#include "mutable/StackT.h"

void swapElems(int *intPointer, int size, int index);

void sum(int size, int *intPointerA, int *intPointerB, int *intPointerC) {
    for (int i = 0; i < size; i++) {
        intPointerC[i] = intPointerA[i] + intPointerB[i];
    }
}

//prec: size > 0
int average(int size, int *intPointer) {
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum = sum + intPointer[i];
    }
    return sum / size;
}

void reverse(int size, int *a) {
    for (int i = 0; i < size / 2; i++) {
        swapElems(a, i, size - 1 - i);
    }
}

int countGreaterThan(int size, int *intPointer, int integer) {
    int count = 0;
    for (int i = 0; i < size; i++) {
        if (intPointer[i] > integer) {
            count++;
        }
    }
    return count;
}

//prec: index, mirrorI son índices válidos del arreglo.
void swapElems(int *intPointer, int index, int mirrorI) {
    int elem = intPointer[index];
    intPointer[index] = intPointer[mirrorI];
    intPointer[mirrorI] = elem;
}

bool isSorted(int size, int *intPointer) {
    bool sorted = true;
    for (int i = 0; i < size - 1; i++) {
        sorted = sorted && (intPointer[i] < intPointer[i + 1]);
    }
    return sorted;
}

//recorrido en profundidad
//operaciones: O(n)
//espacio: O(log n) <=> el árbol está balanceado. Si no, es O(n).
int sumTRecursive(Tree treePointer) {
    if (isEmptyT(treePointer)) {
        return 0;
    }
    return rootT(treePointer) + sumTRecursive(leftT(treePointer)) + sumTRecursive(rightT(treePointer));
}

int sumBFS(Tree tree) {
    if (isEmptyT(tree)) {
        return 0;
    }
    Tree current = tree;
    QueueT pending = emptyQT();
    int sum = 0;
    queueQT(current, pending);
    while (!isEmptyQT(pending)) {
        //obetener el actual
        current = firstQT(pending);
        //prosesa raiz actual
        sum = sum + rootT(current);

        // está procesado y se remueve
        dequeueQT(pending);

        //pasar al sig elem a procesar
        //queue left si no es emptyT
        if (!isEmptyT(leftT(current))) {
            queueQT(leftT(current), pending);
        }
        //queue right si no es emptyT
        if (!isEmptyT(rightT(current))) {
            queueQT(rightT(current), pending);
        }
    }
    return sum;
}

int sumDFS(Tree tree) {
    if (isEmptyT(tree)) {
        return 0;
    }
    Tree current = tree;
    StackT pending = emptyST();
    int sum = 0;
    pushT(current, pending);
    while (!isEmptyST(pending)) {
        //obetener el actual
        current = topT(pending);
        //prosesa raiz actual
        sum = sum + rootT(current);

        // está procesado y se remueve
        popT(pending);

        //pasar al sig elem a procesar
        //queue left si no es emptyT
        if (!isEmptyT(leftT(current))) {
            pushT(leftT(current), pending);
        }
        //queue right si no es emptyT
        if (!isEmptyT(rightT(current))) {
            pushT(rightT(current), pending);
        }
    }
    return sum;
}

// operaciones: O(n)
// espacio: O(1)
int sumQ(Queue queue) {
    int sum = 0;
    init(queue);
    while (hasNext(queue)) {
        sum = sum + current(queue);
        next(queue);
    }
    return sum;
}

int main() {
    Tree t1 = leafT(1);
    Tree t2 = leafT(2);
    Tree t3 = leafT(3);
    Tree t4 = leafT(4);
    Tree t5 = leafT(5);
    Tree t6 = leafT(6);
    Tree t7 = leafT(7);

    setLeftT(t1, t2);

    setLeftT(t2, t4);
    setRightT(t2, t5);

    setRightT(t1, t3);

    setLeftT(t3, t6);
    setRightT(t3, t7);

    Tree t8 = nodeT(1,
                    nodeT(2, leafT(4), leafT(5)),
                    nodeT(3, leafT(6), leafT(7))
    );

    int sum1 = sumTRecursive(t1);
    int sum8 = sumBFS(t8);
    cout << sum1 << endl;
    cout << "BFS sum: " << sum8 << endl;
    cout << "DFS sum: " << sumDFS(t8) << endl;
    return 0;
}
