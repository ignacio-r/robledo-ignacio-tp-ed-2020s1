#include <iostream>
#include <string>
#include "entrenador.h"

// contruye un entrenador
Entrenador mkEntrenador(std::string nombre){
    std::cout << "Falta completar mkEntrenador" << std::endl;
    exit(1);
}

// Agrega dicho pokemon a la lista del entrenador
void capturar(Entrenador entrenador, Pokemon pokemon){
    std::cout << "Falta completar capturar" << std::endl;
    exit(1);
}

// Devuelve la cantidad de pokemon que posee el entrenador.
int cantPokemones(Entrenador entrenador){
    std::cout << "Falta completar cantPokemones" << std::endl;
    exit(1);
}

// Devuelve la cantidad de pokemon de determinado tipo que posee el entrenador.
int cantPokemonesDe(Entrenador entrenador, TipoDePokemon tipoDePokemon){
    std::cout << "Falta completar cantPokemonesDe" << std::endl;
    exit(1);
}

// Dada una array de entrenadores devuelve la concatenacion de todos sus pokemon.
Pokemon* concatPokemon(Entrenador* entrenadores, int cantidadEntrenadores){
    std::cout << "Falta completar concatPokemon" << std::endl;
    exit(1);
}


// Dado un entrenador, devuelve True si posee al menos un pokemon de cada tipo posible.
bool esMaestro(Entrenador entrenador){
    std::cout << "Falta completar esMaestro" << std::endl;
    exit(1);
}

// Dado un entrenador libera la memoria ocupada por el mismo
void destroyEntrenador(Entrenador entrenador){
    std::cout << "Falta completar destroyEntrenador" << std::endl;
    exit(1);

}
